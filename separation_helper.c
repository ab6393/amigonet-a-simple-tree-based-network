//separation_helper.c

//Project 2: Amigonet Social Network

//implements the separation_helper.h file. it implements all the functions prototyped in the header file, allowing the list to be used as intended.

//@author Andrew Baumher

//include statements

#include "separation_helper.h"
//NULL, etc
#include <stdlib.h>

//creates the node. takes a user and sets it to the node's user member. returns a node
listNode * createNode(User * user)
{
	listNode * node = malloc(sizeof(listNode));
	node->user = user;
	node->next = NULL;
	return node;
}

//creates the master list, using given user. user is not part of the masterlist, so it is
//passed to a create node function, which uses it. the node is then used. returns the list
masterList * createList(User * user)
{
	masterList * newList = malloc(sizeof(masterList));
	listNode * temp = createNode(user);
	newList -> first = temp;
	newList -> last = newList->first;
	return newList;
}

//frees all memory in the list. loops while the list is not full, then removes itself.
void destroyList(masterList * list)
{
	//list->first should theoretically not change ever, as the list is added to, but 
	//never removed from, however, upon deletion, this is fine, because the entire list
	//is being removed.
	while (list->first != NULL)
	{
		//temp is the original first to be freed, however, if freed, next would be lost
		//so it is set to temp first. then second (first->next) becomes the new first.
		//then temp is deleted
		listNode * temp = list->first;
		list-> first = list->first->next;
		free(temp);
	}
	//after all is freed, free the list itself.
	free(list);
}

//takes a given master list and searches to see if a given user exists in it. 
//returns 1 if found, 0 if not. 
int findUserInList(masterList * list, User * user)
{
	listNode * check = list-> first;
	while (check != NULL)
	{
		if (check->user == user) return 1;
		check = check->next;
	}
	return 0;
}

//adds a given user to the given list, but does nothing if already in the list. 
//this is done by creating a node to store the user. 
void addToList(masterList * list, User * user)
{
	if (findUserInList(list, user) == 0)
	{
		//create node with given user
		listNode * temp = createNode(user);
		//append it to the end and update the last element pointer. 
		list->last->next = temp;
		list->last = list->last->next;
	}
}

