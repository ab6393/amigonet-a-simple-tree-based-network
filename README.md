# Amigonet 
**A simple tree-based network system.  
Target OS: Linux (should work anywhere). Tested on: Ubuntu, WSL Ubuntu**  
This is a simple application that builds a network of users and their relations to each other, and is built using a low-level tree-like stucture. Conceptually, the idea of this exercise is to mimic how social networking sites like Facebook could store their references to other users, and also provide a degrees of separation tool (for context: a colloquial example of this is Six Degrees of Kevin Bacon).
## Implementation
Users are stored with a forward (singly) linked list of their friends and the relevant data (in this case, just their name) in a struct. Friends are added or removed by adding or removing them from this linked list.  
When finding degrees of separation, a Breadth-First Search is used. Each user stores their friends as a singly linked list, and these are iterated over. If the target is not found, each of their friends dumps their friends into another list and the process repeats. Each time this happens the depth increases by 1. Note that these nodes are marked as visited to prevent infinite loops and to make the overall process more efficient.
## Setup
Users are added via user input, though in practice this is often piped from a text file, as it is easier to do and harder to make mistakes with. Users are registered with **register <name>**, friended or unfriended ith with **(un)friend <name> <friendName>**, and degrees of separation between two users is found with **separation <user1> <user2>**
## Exiting
The program ends when the pipe from input is closed or otherwise unable to provide additional input. 
## Compiling and Usage 
Code is compiled with the attached Makefile. No additional dependencies are required.  
run the program with ./amigosim (linux) or by added .exe to amigosim and running via command line (windows)
### Commands
**register <name>:** Adds a user of Name. If a user with that name already exists, does nothing. 
**friend <user1> <user2>:** creates a friend connection between user1 and user2. if one aleady exists, does nothing.
**unfriend <user1> <user2>:** removes a friend connection between user1 and user2. if no connection exists, does nothing.
**dump:** displays the user network in a human-readable manner. 
**separation <user1> <user2>:** determines the degree of separation in this network between the two users. If no connection exists, returns -1.
## Examples 
Some sample input has been included with scenario1.txt and scenario2.txt  
Results of running scenario1:  
![Example Output](img/Run1.png)
Running scenario2:  
![Example Output](img/Run2.png)
