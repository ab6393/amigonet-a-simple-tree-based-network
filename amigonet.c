//amigonet.c

//Project 2: Amigonet Social Network

//implements the amigonet.h file. 

//Creates the underlying structure for amigonet network. it stores all the users and all 
//of their friends, and allows the users to remove or add friends as needed. it also 
//dynamically allocates itself and has functions for creation and deletion. much of the 
//code, however, is "hidden", of sorts, in various helper files. 

//@author Andrew Baumher

//include statements

#include "helper.h"
#include "separation_helper.h"
//size_t, NULL, etc
#include <stdlib.h>
//printf, etc
#include <stdio.h>
//strncpy, etc
#include <string.h>

//#define MAIN_ELSEWHERE

//static data types to be used as a sort of global objects. unfortunately, due to
//coding constraints placed by the given code, the only way to have an object each of these
//methods can use is to have a singleton or static data types. 
static User ** network;
static int numUsers = 0;

//deals with mallocing the network out, and makes sure that the numUsers is 0.
//idiot proofing, for lack of a better term, would have this method check to see if there
//already was an amigonet before executing, but it is assumed in this lab that this will
//not be an issue.
void create_amigonet()
{
	network = malloc(sizeof(User *));
	numUsers = 0;
}

void destroy_amigonet()
{
	//loops to remove every user in the network
	for (int x = 0; x< numUsers; x++)
	{
		//friends are dynamically created for each user. we must remove them all
		removeAllFriends(network[x]);
		char * removeMe = (char *) network[x]->name;
		//note that in order to pass the const char * through the methods, it needed to be
		//copied. this meant it was dynamically allocated, and is thus freed here. 
		network[x]->name = NULL;
		free (removeMe);
		free (network[x]);
		network[x] = NULL;
	}
	//now frees the network
	free (network);
	network = NULL;
	numUsers = 0;
}

void addUser(const char * name)
{
	//null edge case. we don't want a null string to be added to the list by accident. 
	if (name == NULL) return;
	//copies the name, so it can be used. it is irksome, but it works. note that the malloc
	//means it needs to be freed, which is done in the "destructor" or in a fail state, below
	char * copiedName = malloc(sizeof(char)* strlen(name) + 1);
	strncpy(copiedName, name, strlen(name));
	//adds the null terminator
	copiedName[strlen(name)] = '\0';  

	//checks to see if the user is already there. if not, then adds it
	if (findUser( copiedName) == NULL)
	{
		//reallocs the network to increase its size by one User *. 
		//note how this is done via increasing the number of users, then mallocing it out.
		network = realloc(network, (++numUsers) * sizeof(User *));
		network[numUsers-1] = createUser(copiedName); 
	}
	//if it is in the list, then free the malloc'd string
	else free(copiedName);
}

//uses strcmp to see if the name is already registered. 
//returns the pointer to the user if it exists, otherwise it returns NULL
User * findUser (const char * name)
{
	//loops through all the users in the network
	for(int x = 0; x< numUsers; x++)
	{
		//if they have the same name as the user currently being checked, return a pointer
		//to that specific user. 
		if (strcmp(name, network[x]->name) == 0)
		{
			return network[x];	
		}
	}	
	//if the user is not in the network, return NULL
	return NULL;
}


//takes two users and creates a connection between them, if there isn't one there already.
void addAmigo( User * user, User * amigo)
{
	//checks to see if the user pointers are not null. nothing happens with a null.
	if (user == NULL || amigo == NULL) return;

	//if the user exists in the network. should be taken care of outside the function, 
	//error handling is always good practice.
	if(findUser(user->name) != NULL && findUser(amigo->name) != NULL)
	{
		//checkAmigo checks to see if the given user has said amigo, and if not, adds it
		checkAmigo(user, amigo);
		//note that this needs to be done for both users, so this is the done for amigo
		//it was redundant to rewrite the code, and it allows for less human error.
		checkAmigo(amigo, user);
	}
}


//takes two users, and if they exist and are friends with one another, "disconnects" them
//so that they are no longer friends. returns nothing, but affects the global vars. 
void removeAmigo( User * user, User * ex_amigo)
{
	if (user == NULL || ex_amigo == NULL) return;
		
	//if the user exists in the network. should be taken care of outside the function, 
	//error handling is always good practice.
	if(findUser(user->name) != NULL && findUser(ex_amigo->name) != NULL)
	{
		findAndRemove(user, ex_amigo);
		//this is bidirectional, so it needs to be done in reverse. as with the add amigo
		//a separate helper was created to limit error and make it less redundant. 
		findAndRemove(ex_amigo, user);
	}
}


//takes two users, and finds the degrees of separation between them, as defined by the
//given examples. much of this code utilizes code in another separate helper file,
//created to make this code more readable. 
size_t separation (const User * user1, const User * user2)
{
	//depth will be returned later.
	size_t depth = 0;
	//edge detection. the users must exist and have friends for them to be connected.
	//if neither case is true, then it should return -1.
	//note that the users are found first in the amigosim.h file, so their existance is 
	//guarenteed. if not, findUser could be called on both to see if they are in the net
	if (user1 == NULL || user2 == NULL || 
			user1->amigos == NULL || user2->amigos ==NULL)
	{
		return -1;
	}
	//if the users are not the same, keep going
	if (user1 != user2)
	{
		//create a "master" list. it is a linked list that stores all the Users checked
		//it also stores all users to check and the current one being checked. 
		masterList * theList = createList((User *) user1);
		//the masterList "creator" takes user1, the last user of the depth 0. therefore, we
		//need to increment depth. 
		depth++;
		//take the first node, which we will check. current will update to the next item in
		//the list when the loop is done 
		listNode * current = theList->first;
		listNode * lastOfCurrentDepth = theList->last;

		//loops while current is still in the list. when it is null, the entire list is 
		//exhausted, and there is no way to connect these users via friends. 
		while (current != NULL)
		{
			//each user has a list of friends, which amigos points to. 
			Friends checkFriends = current->user->amigos;

			//this will loop through the friends list until no more friends are left.
			while (checkFriends != NULL)
			{
				//if the user is found
				if (checkFriends->amigo == user2)
				{
					//free all allocated memory for this list struct.
					destroyList(theList);
					return depth; 	
				}
				//otherwise, add the current user to the checked list
				addToList(theList, checkFriends->amigo);
				//go to the next friend.
				checkFriends = checkFriends->next;
			}
			//if the current user, which was just checked, is the last of the current depth
			if (current == lastOfCurrentDepth)
			{
				//update the last of the current depth to the last element. which is the
				//last of the depth on the next level. 
				lastOfCurrentDepth = theList->last;
				//increase the depth to account for this change. 
				depth++;	
			}
			//go to the next element in the master list. 
			current = current->next;
		}
		//if this is reached, then there are no connections between them, of any length.
		//free the list, and return -1, or no connection. 
		destroyList(theList);
		return -1;
	}
	//otherwise, return the default depth of 0.
	return depth;	
}


//dump data. prints out the network, in the specified formatting. 
void dump_data()
{
	printf("\n");
	//for every user in the network
	for (int x = 0; x < numUsers; x++)
	{
		//get and print that user
		User * printMe = network[x];
		printf("User %s; friends:", printMe->name);
		Friends printFriends = printMe->amigos;
		//then, loop through their entire friends list, printing out all their friends
		while (printFriends != NULL)
		{
			printf(" %s", printFriends->amigo->name);
			printFriends = printFriends->next;
		}
		printf("\n");
	}
	printf("\n");
}
// a debug main. it was used with a conditional statement, which the make was not fond
//of. it is here to show that i did do testing, but commented out so it will compile for
//submission. 

/*
#ifndef MAIN_ELSEWHERE
int main()
{
	return 0;	
}
#endif*/
