//helper.h

//Project 2: Amigonet Social Network

//prototypes a series of helper fucntions to be used in amigonet.c it also defines the friends struct that was previously typedef'd by the amigonet.h header file.

//@author Andrew Baumher

//include statements

#ifndef HELPER_H
#define HELPER_H
//needed to define size_t in the amigonet.h file. serves no other purpose
#include <stddef.h>
#include "amigonet.h"

//struct Friends_Struct, which was used in the Users struct, which was defined in amigonet.h
struct Friends_struct{
    Friends next;
    User * amigo;
};

//creates a Friends struct, from a user. it will return a Friends data type
Friends createFriends(User * amigo);

//Remove all friends removes all amigos from a given user, setting it to NULL
//it als frees all memory associated with it.
void removeAllFriends(User * person);

//Creates a User and returns it' location in memory. it takes the name of the user and
//assigns it to the User's name member. 
User * createUser(char * name);

//a helper for find_amigo that will check if one user has the other as a friend and if not
//connects the two. it is one directional. 
void checkAmigo (User * user, User * amigo);

//a helper for disconnect that will check if one user has the other as a friend and if so
//removes the Friends node indication the two are friends. it is one directional. 
//this node is also freed. 
void findAndRemove(User * user, User * ex_amigo);

#endif
