//separation_helper.h

//Project 2: Amigonet Social Network

//A helper file, specifically a header file which sets up a linked list and various methods
//that aid in using this linked list. the list is used to store users as they are found
//to aid in fnding the degrees of separation between two users. 

//@author Andrew Baumher

#ifndef SEP_HELPER
#define SEP_HELPER

//needed to define size_t in amigonet.h. it has no other purpose.
#include <stddef.h>
//include statements it depends on amigonet for the user struct. 
#include "amigonet.h"

//a struct of "Nodes" used to make up the list
//next is a pointer to another listNode. it points to the next element in the list, or null
//if no more elements exist. user points to a User struct that contains the specific user 
//that is to be added to the list. 
typedef struct listNode
{
	struct listNode * next;
	User * user;
} listNode;

//a struct that acts like a linked list. it stores the first and last items in the list. 
typedef struct masterList
{
	listNode * first;
	listNode * last;

} masterList;

//creates a single node it takes a user * which it sets to the user attribute.
//it returns this node. 
listNode * createNode(User * user);

//creates the master linked list. it creates a node based on user and then sets all the 
//pointers to this node. it returns this list. 
masterList * createList(User * user);

//destroys the given Linked List and all the nodes it contains, freeing all the memory. 
void destroyList(masterList * list);

//searches through the given master Linked List and sees if the user is in it. 
//the integer acts like a boolean, returning "true" if it is there, "false" if not.
int findUserInList(masterList * list, User * user);

//adds a given user to the master list. it does so by creating a new node with the user
void addToList(masterList * list, User * user);

#endif

