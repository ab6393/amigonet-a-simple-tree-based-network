//helper.c

//Project 2: Amigonet Social Network

//implements the helper.h file.


//Methods that are used to aid the amigonet functions (aside from separation, which was
//large enough to be placed separately), are placed here. these methods are used to limit 
//redundant uses of the same code, and to make the code more elegant. 

//@author Andrew Baumher

//include statements

#include "helper.h"
//NULL, etc.
#include <stdlib.h>

//creates a Friends struct, and returns it. it sets the amigo member of the friends struct
//to the given user. 
Friends createFriends(User * amigo)
{

	Friends temp = malloc (sizeof (struct Friends_struct));
	temp->next = NULL;
	temp->amigo = amigo;
	return temp;
}

//removes all friends via iteration, not searching for a specific one, one at a time.
//it is more efficient and cleaner this way. 
void removeAllFriends (User * person)
{
	//get the friend, then loop through each friend until none are left.
	Friends temp = person->amigos;
	while (temp != NULL)
	{
		//set aside the current friend, increment to the next friend, then free the friend
		//that was previously set aside
		Friends temp2 = temp;
		temp = temp-> next;
		free (temp2);
	}
}

//Creates a user. takes the name of the user, and sets it to the user's name member. 
//then returns the user.
User * createUser(char * name)
{
	User * returnUser = malloc (sizeof(User));
	returnUser->name = name;
	returnUser->amigos = NULL;
	return returnUser;
}

//checks to see if two users are friends, and if so, adds a connection from the user to
//the amigo and returns. if not, does nothing and returns.
void checkAmigo(User * user, User * amigo)
{
	Friends temp = user->amigos;
	Friends previous = NULL;

	while (temp != NULL)
	{
		//if already in the friends list, do nothing
		if (temp->amigo == amigo) return;
		else
		{
			previous = temp;
			temp = temp->next;
		}
	}
	//if not found, add it. 
	temp = createFriends(amigo);
	//edge case detection: if previous is still null, it means that the while loop never 
	//ran. thus, this is the first friend added. if this is not here, it will seg fault
	//when you point to NULL->next.  
	if (previous == NULL)
	{
		user->amigos = temp;
	}

	//originally, i had the new friends append to the end. the supplied output
	//had the new friends append to the beginning. i don't believe this matters,
	//but i adjusted the code anyway. my original is commented out below.

	//else previous-> next = temp;

	//the adjusted version that appends it at the beginning. 
	else 
	{
		Friends oldFirst = user->amigos;
		user->amigos = temp;
		user->amigos->next = oldFirst;
	}
	
}

//takes two users, and if they are friends, removes this connection. otherwise does nothing
//note that when this is called, it is known that the user and ex amigo are both not null.
void findAndRemove(User * user, User * ex_amigo)
{
	Friends temp = user->amigos;
	Friends previous = NULL;
	while (temp != NULL)
	{
		//if in the friends list, remove it and exit
		if (temp->amigo == ex_amigo)
		{
			Friends removeMe = temp;
			temp = temp->next;
			free(removeMe);
			//if on the first execution of the while loop, and therefore, the first friend
			//then we will want to make sure user points to the new first. after all, it 
			//cannot set NULL->next to temp.
			if (previous == NULL)
			{
				user->amigos = temp;
			}
			else previous->next = temp;
			break;
		}
		//otherwise, increment temp and previous and continue the loop.
		else
		{
			previous = temp;
			temp = temp->next;
		}
	}
	//Note that if the code reaches here, then they were not friends anyway, and will exit.
}
